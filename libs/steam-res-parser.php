<?php

abstract class SteamResToken {
	const T_WHITESPACE  = 1;
	const T_BLOCK_START = 2;
	const T_BLOCK_END   = 3;
	const T_STRING      = 4;
	const T_VALUE       = 5;
	const T_PLATFORM    = 6;
	const T_COMMENT     = 7;
	const T_BASE        = 8;
	const T_INT         = 9;

	static function GetTokens() {
		return [
			new Token(static::T_WHITESPACE,  '/^\s+/',            "T_WHITESPACE", true),
			new Token(static::T_BLOCK_START, '/^\{/',             "T_BLOCK_START"),
			new Token(static::T_BLOCK_END,   '/^\}/',             "T_BLOCK_END"),
			new Token(static::T_STRING,      '/^"[^"]*"/',        "T_STRING"),
			new Token(static::T_VALUE,       '/^[A-Za-z][\w.]*/', "T_VALUE"),
			new Token(static::T_INT,         '/^\d+/',            "T_INT"),
			new Token(static::T_PLATFORM,    '/^\[!?\$\w+\]/',    "T_PLATFORM"),
			new Token(static::T_COMMENT,     '/^\/\/.*/',         "T_COMMENT", true),
			new Token(static::T_BASE,        '/^#base.*/',        "T_BASE"),
		];
	}
}

class SteamResObject {
	protected $key;
	protected $value;
	protected $platform;

	const COLUMN_WIDTH = 32;

	public function __construct($key = null, $value = null, $platform = null) {
		$this->key = $key;
		$this->value = $value;
		$this->platform = $platform;
	}

	public function getKey() {
		return $this->key;
	}
	public function setKey(string $key) {
		$this->key = $key;
	}

	public function indexOf($key) {
		foreach($this->value as $index => $value) {
			if ($value->getKey() === $key) {
				return $index;
			}
		}

		return null;
	}

	public function getValue($index = null) {
		if (is_null($index)) {
			return $this->value;
		}

		if (! is_array($this->value)) {
			throw new Exception("value is not an array");
		}

		if (array_key_exists($index, $this->value)) {
			return $this->value[$index];
		} elseif (is_string($index)) {
			$i = $this->indexOf($index);
			if (array_key_exists($i, $this->value)) {
				return $this->value[$i];
			}
		}

		return null;
	}
	public function setValue($value, $index = null) {
		if (! is_null($index) && ! is_array($this->value)) {
			throw new Exception("value is not an array");
		}

		if (is_null($index)) {
			$this->value = $value;
		} else {
			$this->value[$index] = $value;
		}
	}
	public function addValue(SteamResObject $value) {
		if (! is_array($this->value) && ! is_null($this->value)) {
			throw new Exception("value is not an array");
		}

		if (is_null($this->value)) {
			$this->value = [];
		}
		$this->value[] = $value;
	}

	public function getPlatform() {
		return $this->platform;
	}
	public function setPlatform(string $platform) {
		$this->platform = $platform;
	}

	protected function AddTabs($input, $tabs = 1) {
		if (is_array($input)) {
			return array_map(function($v) use ($tabs) {
				return static::AddTabs($v, $tabs);
			}, $input);
		} elseif (is_string($input)) {
			return str_pad('', $tabs, "\t", STR_PAD_LEFT) . $input;
		}
	}

	protected function SerialiseToArray(): array {
		$output = null;

		if (is_string($this->value) || is_null($this->value)) {
// 			$key = str_pad("\"{$this->key}\"", static::COLUMN_WIDTH, ' ');
			$output = "\"{$this->key}\" \"{$this->value}\"";

			if ($this->platform) {
				$output .= " {$this->platform}";
			}
			$output = [$output];
		} elseif ($this->value instanceof SteamResObject) {
			$output = static::AddTabs($this->value->SerialiseToArray());
		} elseif (is_array($this->value)) {
			$output = [];
			$key = '"' . $this->key . '"';
			if ($this->platform) {
				$key .= " {$this->platform}";
			}
			$output[] = $key;
			$output[] = '{';
			foreach ($this->value as $value) {
				$output = array_merge(
					$output,
					static::AddTabs($value->SerialiseToArray())
				);
			}
			$output[] = '}';
		} else {
			throw new Exception(var_dump($this));
		}

		return $output;
	}

	public function Serialise(): string {
		return implode("\n", $this->SerialiseToArray());
	}
}

class SteamResFile {
	protected $bases;
	protected $schemes;

	public function __construct() {
		$this->bases = [];
		$this->schemes = [];
	}

	public function addBase(string $base) {
		$this->bases[] = $base;
	}

	public function getBase($index) {
		if (array_key_exists($index, $this->bases)) {
			return $this->bases[$index];
		}
		return null;
	}

	public function getScheme($index) {
		if (array_key_exists($index, $this->schemes)) {
			return $this->schemes[$index];
		} elseif (is_string($index)) {
			$i = $this->indexOf($index);
			if (array_key_exists($i, $this->schemes)) {
				return $this->schemes[$i];
			}
		}
		return null;
	}
	public function setScheme($scheme, $index) {
		$this->schemes[$index] = $scheme;
	}
	public function addScheme(SteamResObject $scheme) {
		$this->schemes[] = $scheme;
	}

	public function indexOf($key) {
		foreach($this->schemes as $index => $scheme) {
			if ($scheme->getKey() === $key) {
				return $index;
			}
		}

		return null;
	}

	public function Serialise(): string {
		$output = [];

		foreach ($this->bases as $base) {
			$output[] = $base;
		}

		foreach ($this->schemes as $scheme) {
			$output[] = $scheme->Serialise();
		}

		$output[] = "";

		return implode("\n", $output);
	}
}

class SteamResParser {
	static public function Parse(string $string) {
		$lexer = new Lexer(SteamResToken::GetTokens());
		$tokens = $lexer->Tokenise($string);

		$file = new SteamResFile();

		while ($result = static::PeekForBase($tokens)) {
			next($tokens);
			$file->addBase($result);
		}

		while ($result = static::LookForKey($tokens)) {
			$file->addScheme($result);
		}

		return $file;
	}

	static protected function ExtractStringContents(string $string) {
		$matches = null;
		if (preg_match('/"([^"]*)"/', $string, $matches)) {
			return $matches[1];
		}

		return null;
	}

	static protected function PeekForBase(&$tokens) {
		$token = current($tokens);
		if (! $token) {
			return null;
		}

		switch ($token->getKey()) {
			case SteamResToken::T_BASE:
				return $token->getMatch();
			default:
		}
	}

	static protected function LookForKey(&$tokens) {
		$token = current($tokens);
		next($tokens);
		if (! $token) {
			return null;
		}

		$key = null;
		switch ($token->getKey()) {
			case SteamResToken::T_STRING:
				$key = static::ExtractStringContents($token->getMatch());
			case SteamResToken::T_VALUE:
				$key = $key ?? $token->getMatch();
				$data = static::LookForValueOrPlatform($tokens);
				$data->setKey($key);
				return $data;
			case SteamResToken::T_BLOCK_END:
				return null;
			default:
				throw new UnexpectedTokenException($token);
		}
	}

	static protected function LookForValueOrPlatform(&$tokens) {
		$token = current($tokens);
		next($tokens);
		if (! $token) {
			return null;
		}

		$data = new SteamResObject();
		$value = null;
		switch ($token->getKey()) {
			case SteamResToken::T_STRING:
				$value = static::ExtractStringContents($token->getMatch());
			case SteamResToken::T_INT:
			case SteamResToken::T_VALUE:
				if ($result = static::PeekForPlatform($tokens)) {
					$data = $result;
					next($tokens);
				}
				$data->setValue($value ?? $token->getMatch());
				return $data;
			case SteamResToken::T_BLOCK_START:
				$data->setValue([]);
				while ($result = static::LookForKey($tokens)) {
					$data->addValue($result);
				}
				return $data;
			case SteamResToken::T_PLATFORM:
				$data = static::LookForValueOrPlatform($tokens);
				$data->setPlatform($token->getMatch());
				return $data;
			default:
				throw new UnexpectedTokenException($token);
		}
	}

	static protected function PeekForPlatform(&$tokens) {
		$token = current($tokens);
		if (! $token) {
			return null;
		}

		switch ($token->getKey()) {
			case SteamResToken::T_PLATFORM:
				$data = new SteamResObject();
				$data->setPlatform($token->getMatch());
				return $data;
			default:
				return null;
		}
	}
}
