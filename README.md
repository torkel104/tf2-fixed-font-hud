All fonts are replaced with Raleway and DejaVu Sans Mono

To change fonts edit `fonts.json` and then run `php font-changer.php` and don't forget to put your fonts in `resource/fonts/`
